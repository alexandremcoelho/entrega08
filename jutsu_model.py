class Jutsu():
    jutsu_ranks = ( 'D', 'C', 'B', 'A', 'S')
    def __init__(self,jutsu_name,jutsu_type,jutsu_level,jutsu_damage,chakra_spend):
        self.jutsu_name = jutsu_name
        self.jutsu_type = jutsu_type
        jutsu_level = [rank for rank in self.jutsu_ranks if jutsu_level.upper() == rank][0]
        if(jutsu_level):
            self.jutsu_level = jutsu_level
        else:
            self.jutsu_level = 'Unranked'
        self.jutsu_damage = jutsu_damage
        if(chakra_spend > 0):
            self.chakra_spend = chakra_spend
        else:
            self.chakra_spend = 100

# rasengan = Jutsu('Rasengan', 'Vento', 'a', 20, -15)

# print(rasengan.__dict__)