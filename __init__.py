from jutsu_model import Jutsu
from ninja_model import Ninja
from jounin_model import Jounin

rasengan = Jutsu('Rasengan', 'Vento', 'a', 20, -15)

print(rasengan.__dict__)

naruto = Ninja('Naruto', 'Uzumaki', 'Konoha')

print(naruto.__dict__)

res = naruto.learn_jutsu(rasengan)
print(res)

kakashi_proficiency = {'taijutsu': 7, 'ninjutsu': 8, 'genjutsu': 4}

kakashi = Jounin('Kakash', 'Hatake', 'Konoha', kakashi_proficiency)
print(kakashi.__dict__)

res = kakashi.list_best_proficiency()
print(res)

res = kakashi.start_mission()
print(res)

res = kakashi.return_from_mission()
print(res)

rasengan = Jutsu('Rasengan', 'Vento', 'a', 20, -15)

naruto = Ninja('Naruto', 'Uzumaki', 'Konoha')

naruto.learn_jutsu(rasengan)

sasuke = Ninja('Sasuke', 'Uchiha', 'Konoha')

res = naruto.cast_jutsu(rasengan, sasuke)
print(res)

